package main

import "github.com/zserge/webview"

func main() {
	debug := true
	w := webview.New(debug)
	defer w.Destroy()
	w.SetTitle("Minimal webview example")
	w.SetSize(800, 600, webview.HintNone)

	// Webrtc
	//w.Navigate("http://127.0.0.1:8080/?room=1111")
	//w.Navigate("https://meet.google.com/wnn-krwe-yzq")

	// Vidoe
	w.Navigate("https://www.youtube.com/channel/UCWv10Q9JJ0ztqSTSs7GRxaw")

	// basic
	//w.Navigate("https://en.m.wikipedia.org/wiki/Main_Page")

	w.Run()
}
