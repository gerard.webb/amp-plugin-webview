
GO_OS 		:= $(shell go env GOOS)
GO_ARCH		:= $(shell go env GOARCH)

GIT_VERSION	:= $(shell git describe --tags)

SAMPLE_FSPATH = $(PWD)/example
SAMPLEWEBVIEW_FSPATH = $(PWD)/examplewebview

help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf " \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


print: ## print
	@echo
	@echo LIB_NAME: $(LIB_NAME)
	@echo LIB: $(LIB)
	@echo LIB_BRANCH: $(LIB_BRANCH)
	@echo LIB_FSPATH: $(LIB_FSPATH)
	@echo

	@echo
	@echo SAMPLE_FSPATH: $(SAMPLE_FSPATH)
	@echo SAMPLEWEBVIEW_FSPATH: $(SAMPLEWEBVIEW_FSPATH)
	@echo



os-dep:
	# Assume hover make file has ben run

### FLU

flu-config: ## flu-config
	flutter channel beta
	flutter upgrade --force

flu-web-run: ## flu-web-run
	# WORKS :)
	flutter config --enable-web
	cd $(SAMPLE_FSPATH) && flutter run -d chrome

flu-web-build: ## flu-web-build
	# WORKS :)
	flutter config --enable-web
	cd $(SAMPLE_FSPATH) && flutter build web

flu-mob-run: ## flu-mob-run
	# WORKS :)
	cd $(SAMPLE_FSPATH) && flutter run -d all

flu-desk-run: ## flu-desk-run

	#cd $(SAMPLE_FSPATH) && hover init
	cd $(SAMPLE_FSPATH) && hover run

flu-desk-build: ## flu-desk-build

	#cd $(SAMPLE_FSPATH) && hover init
	#cd $(SAMPLE_FSPATH) && hover build darwin

flu-desk-pack-init: ## flu-desk-pack-init

	# NOTE Copy in the right PLIST variables to allow Webrtc work
	
	#darwin
	cd $(SAMPLE_FSPATH) && hover init-packaging darwin-pkg

flu-desk-pack: ## flu-desk-pack
	# skaffold the file system with the pkg templates.
	cd $(SAMPLE_FSPATH) && hover build darwin-pkg

	# inject out .app into it.
	#cp -rf $(current_dir)/desktop/output/darwin-amd64/ION\ Desktop\ App.app ./Applications/maintemplate.app





### GEN 

flu-update:
	cd $(SAMPLE_FSPATH) && flutter packages get
	$(MAKE) gen-icons
	$(MAKE) gen-hive
	$(MAKE) gen-proto
	cd $(SAMPLE_FSPATH) && flutter analyze 


gen-icons:
	# mobile and web
	@echo
	@echo Generating icons for Flutter
	@echo
	cd $(SAMPLE_FSPATH) && flutter pub run flutter_launcher_icons:main

	# desktop
	@echo
	@echo Copying icon-png from flutter assets into go assets, so hover can use it
	@echo
	cp $(SAMPLE_FSPATH)/assets/icon/icon.png $(SAMPLE_FSPATH)/go/assets

gen-hive:
	cd $(SAMPLE_FSPATH) && flutter packages pub run build_runner build --delete-conflicting-outputs

gen-proto:
	pub global activate protoc_plugin
	mkdir -p $(SAMPLE_FSPATH)/lib/api/v1/google/protobuf

ifeq ($(GO_OS), windows)
	@echo Windows detected
	protoc empty.proto timestamp.proto wrappers.proto --proto_path=$(LIB_FSPATH)/server/third_party/google/protobuf/ --plugin=$(HOME_PATH)/AppData/Roaming/Pub/Cache/bin/protoc-gen-dart.bat --dart_out=grpc:"$(PROTO_OUTPUT)/client/lib/chat_view/api/v1/google/protobuf"
	protoc chat.proto --proto_path=$(LIB_FSPATH)/server/api/proto/v1/ --plugin=$(HOME_PATH)/AppData/Roaming/Pub/Cache/bin/protoc-gen-dart.bat --dart_out=grpc:"$(PROTO_OUTPUT)/client/lib/chat_view/api/v1/"
else
	protoc empty.proto timestamp.proto wrappers.proto --proto_path=$(LIB_FSPATH)/server/third_party/google/protobuf --plugin=protoc-gen-dart=$(HOME)/.pub-cache/bin/protoc-gen-dart --dart_out=grpc:$(SAMPLE_FSPATH)/lib/api/v1/google/protobuf
	protoc chat.proto --proto_path=$(LIB_FSPATH)/server/api/proto/v1/ --plugin=protoc-gen-dart=$(HOME)/.pub-cache/bin/protoc-gen-dart --dart_out=grpc:$(SAMPLE_FSPATH)/client/lib/chat_view/api/v1/
endif


### ASTI

# 1. dep
# 2. build
# 3. run (for the desktop your on)
# 4. pack (for the desktop your on)
# 5. sign (for the desktop your on)

#export GO111MODULE=off
ASTI_BUND_PATH=github.com/asticode/go-astilectron-bundler
ASTI_BUND_FSPATH=$(GOPATH)/src/$(ASTI_BUND_PATH)
ASTI_BUND_BIN=$(GOPATH)/bin/astilectron-bundler

ASTI_BOOT_PATH=github.com/asticode/go-astilectron-bootstrap
go-desk-dep: ## go-desk-dep
	# as per the readme: https://github.com/asticode/go-astilectron-bundler#installation  !!
	@echo ASTI_BUND_PATH: $(ASTI_BUND_PATH)
	@echo ASTI_BUND_FSPATH: $(ASTI_BUND_FSPATH)
	@echo ASTI_BUND_BIN: $(ASTI_BUND_BIN)

	go get -u $(ASTI_BUND_PATH)/...
	go get -u $(ASTI_BOOT_PATH)
	#cd $(ASTI_BUND_FSPATH) && go mod init
	cd $(ASTI_BUND_FSPATH)/astilectron-bundler && go install .

	#go install github.com/asticode/go-astilectron-bundler/astilectron-bundler
	#cd $(GOPATH)/bin && go install github.com/asticode/go-astilectron-bundler/astilectron-bundler
	which astilectron-bundler

go-desk-dep-clean: ## go-desk-dep-clean

	rm $(ASTI_BUND_BIN)

go-desk-flu-web: ## go-desk-flu-web
	# flutter build
	flutter config --enable-web
	cd $(SAMPLEWEBVIEW_FSPATH) && flutter build web
	# copy flutter web build to desktop/resources/app
	rm -rf $(SAMPLE_FSPATH)desktop/resources/app
	mkdir -p $(SAMPLE_FSPATH)desktop/resources/app/
	cp -r $(SAMPLE_FSPATH)build/web/* $(SAMPLE_FSPATH)desktop/resources/app

go-desk-build: ## go-desk-build

	# -d is for darwin
	cd $(SAMPLE_FSPATH)/desktop && astilectron-bundler

go-desk-build-run: ## go-desk-build-run

	open $(SAMPLE_FSPATH)/desktop/output/darwin-amd64/IONDesktopApp

go-desk-build-clean: ## go-desk-build
	rm -rf $(SAMPLE_FSPATH)/desktop/output






	